function OK = isGpuAvailable

% This function determines if there is a usable GPU available
try
    d = gpuDevice;
    OK = d.SupportsDouble;
catch
    OK = false;
end