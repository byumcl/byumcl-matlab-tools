function stationary = LTF(nonstationary)
% Version 1.0, written by Kerk Phillips
%  This filter removes a linear trend from a time series

[nobs,nvar] = size(nonstationary);
x = ones(nobs,2);
for i=1:nobs
    x(i,2) = i;
end
y = nonstationary;
bet = (x'*x)^(-1)*x'*y;
stationary = y - x*bet;